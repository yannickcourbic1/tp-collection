package core.Class;

import java.util.ArrayList;
import java.util.Scanner;

public class IHM {

    public static Scanner scan = new Scanner(System.in);

    public static void start() {
        String name;
        String strClient;
        Integer numberInteger;
        Integer nbrChr;
        int max = 4;
        int min = 1;
        int range = max - min + 1;
        int maxTarif = 100;
        int minTarif = 30;
        int rangeTarif = maxTarif - minTarif + 1;
        ArrayList<Chambre> chambres = new ArrayList<Chambre>();
        ArrayList<Chambre> choicesChambre = new ArrayList<Chambre>();
        ArrayList<Client> clients = new ArrayList<Client>();
        ArrayList<Reservation> reservations = new ArrayList<Reservation>();
        Hotel h = new Hotel();
        System.out.println("Quel est le nom de l'hotel ?");
        name = scan.next();
        h.setName(name);

        /** GENERATION DES CHAMBRES HOTEL */
        System.out.println("Quel le nombre de chambre dans votre hotel ? ");
        nbrChr = scan.nextInt();
        for (int i = 0; i < nbrChr; i++) {
            Chambre c = new Chambre();
            c.setNumberOfBed((int) (Math.random() * range) + min);
            c.setTarif((Math.random() * rangeTarif) + minTarif);
            c.setStatut(true);
            chambres.add(c);
        }

        for (Chambre c : chambres) {
            System.out.println(c.toString());
        }
        /** CREATION CLIENT */
        Client client = new Client();
        System.out.println("Quel est votre nom ?");
        strClient = scan.next();
        client.setName(strClient);
        System.out.println("Quel est votre prénom ?");
        strClient = scan.next();
        client.setPrename(strClient);
        System.out.println("Quel est votre numéro de téléphone ?");
        strClient = scan.next();
        client.setPhone(strClient);
        System.out.println(client.toString());
        /** CHOIX DE OU DES CHAMBRES */
        System.out.println("Veuillez choisir vos chambres , pour arrêter la saisie , tapez 'stop'");
        numberInteger = scan.nextInt();
        if (numberInteger >= 0) {
            if (chambres.get(numberInteger).getNumber() == numberInteger) {
                chambres.get(numberInteger).setStatut(false);
            }
            choicesChambre.add(chambres.get(numberInteger));
        }
        while (numberInteger >= 0) {
            numberInteger = scan.nextInt();
            if (numberInteger >= 0) {
                if (chambres.get(numberInteger).getNumber() == numberInteger) {
                    chambres.get(numberInteger).setStatut(false);
                }
                choicesChambre.add(chambres.get(numberInteger));
            }
        }
        if (numberInteger < 0) {
            System.out.println("saisie terminée");
        }
        System.out.println("**************************************");
        /** CHAMBRES TOTAUX */
        for (Chambre c : chambres) {
            if (c.getStatut() == false) {
                System.out.println(c.toString());
            }
        }
        System.out.println("****************************************");
        /** CHAMBRES CHOISIE */
        for (Chambre c : choicesChambre) {
            System.out.println(c.toString());
        }

        /** CHAMBRES CHOISIE , MISE EN PLACE DE LA RESERVATION */
        Reservation reservation = new Reservation();
        reservation.setClient(client);
        reservation.setListOfBed(choicesChambre);
        reservation.setStatus(true);

        reservation.resumeStringReservation();
        reservations.add(reservation);
        /** ENREGISTRER ET ACTUALISER HOTEL */
        clients.add(client);
        h.setChambres(chambres);
        h.setClients(clients);
        h.setReservations(reservations);

        /** AFFICHAGE TOTAL DE L'HOTEL */

        h.resumeHotel();

    }

}
