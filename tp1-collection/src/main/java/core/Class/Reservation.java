package core.Class;

import java.util.List;

public class Reservation {

    private Integer id;
    private boolean status;
    private List<Chambre> listOfBed;
    private Client client;

    private static Integer count = -1;

    public Reservation() {
        this.id = ++count;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Chambre> getListOfBed() {
        return this.listOfBed;
    }

    public void setListOfBed(List<Chambre> listOfBed) {
        this.listOfBed = listOfBed;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getDisplayChamber() {
        for (Chambre c : getListOfBed()) {
            System.out.println(c.toString());
        }
        return "";
    }

    public void resumeStringReservation() {
        Double som = 0.0;
        System.out.println("++++++++++++++++++++++++++");
        System.out.println("Id de la réservation : " + this.getId());
        System.out.println("++++++++++++++++++++++++++++++++++++++++");
        System.out.println("Statut de la réservation : "
                + ((getStatus() == false) ? "Réservation annulée ou terminée" : "Réservation en cours"));
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("Liste de chambre réservé : ");
        for (Chambre chambre : getListOfBed()) {
            System.out.println(chambre.toString());
            som += chambre.getTarif();
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("Info Client");
        System.out.println(client.toString());
        System.out.println("++++++++++++++++++++++++++++++++++++");
        System.out.println("le prix total de la réservation est " + som + "€");

    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'\n" +
                ", status='" + ((getStatus() == false)
                        ? "Réservation annulée ou terminée"
                        : "Réservation en cours")
                + "'\n" +
                ", liste de chambre réservée : '" + getDisplayChamber() + "'\n" +
                ", client='" + getClient() + "'\n" +
                "}";
    }

}
