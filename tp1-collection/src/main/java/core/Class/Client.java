package core.Class;

public class Client {

    private Integer id;
    private String name;
    private String Prename;
    private String phone;

    private static Integer count = -1;

    public Client() {
        this.id = count++;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrename() {
        return this.Prename;
    }

    public void setPrename(String Prename) {
        this.Prename = Prename;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "{" +
                ", name='" + name + "'\n" +
                ", Prename='" + Prename + "'\n" +
                ", phone='" + phone + "'\n" +
                "}";
    }
}
