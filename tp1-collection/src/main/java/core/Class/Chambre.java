package core.Class;

public class Chambre {
    private Integer number;
    private boolean statut;
    private Integer numberOfBed;
    private Double tarif;

    private static Integer nbrChr = -1;

    public Chambre() {
        this.number = ++nbrChr;
    }

    public Integer getNumber() {
        return this.number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public boolean isStatut() {
        return this.statut;
    }

    public boolean getStatut() {
        return this.statut;
    }

    public void setStatut(boolean statut) {
        this.statut = statut;
    }

    public Integer getNumberOfBed() {
        return this.numberOfBed;
    }

    public void setNumberOfBed(Integer numberOfBed) {
        this.numberOfBed = numberOfBed;
    }

    public Double getTarif() {
        return this.tarif;
    }

    public void setTarif(Double tarif) {
        this.tarif = tarif;
    }

    @Override
    public String toString() {
        return "{" +
                " number='" + getNumber() + "'\n" +
                ", statut='" + isStatut() + "'\n" +
                ", numberOfBed='" + getNumberOfBed() + "'\n" +
                ", tarif='" + getTarif() + "'\n" +
                "}";
    }
}
