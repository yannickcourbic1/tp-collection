package core.Class;

import java.util.List;

public class Hotel {
    private String name;
    private List<Client> clients;
    private List<Chambre> chambres;
    private List<Reservation> reservations;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Client> getClients() {
        return this.clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public List<Chambre> getChambres() {
        return this.chambres;
    }

    public void setChambres(List<Chambre> chambres) {
        this.chambres = chambres;
    }

    public List<Reservation> getReservations() {
        return this.reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public void resumeHotel() {
        System.out.println("RESUME HOTEL ++++++++++++++++++++++++++++++++");
        System.out.println("Nom de l'hôtel : " + this.getName());
        System.out.println("**NOMBRE DE CLIENT");
        for (Client c : this.getClients()) {
            System.out.println(c.toString());
        }
        System.out.println("NOMBRE DE RESERVATION +++++++++++++++");
        for (Reservation r : this.getReservations()) {
            r.resumeStringReservation();
        }

        System.out.println("NOMBRE TOTAL DE CHAMBRE");
        for (Chambre c : chambres) {
            System.out.println(c.toString());
        }

        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++");
    }
}
